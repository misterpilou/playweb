const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const crawler = require('./puppet.js')

let workers = []
let urls = []

if (cluster.isMaster) {
	console.log(`Master ${process.pid} is running`)

	for (let i = 0; i < numCPUs; i++) {
		let worker = cluster.fork();
        workers.push(worker)
	}
    
    urls = ["https://kaggle.com", "http://apache.org", "https://www.kdnuggets.com", "https://dataversity.net"]
    
	cluster.on('exit', (worker, code, signal) => {
		console.log(`worker ${worker.process.pid} died`);
	});
    for (const id in cluster.workers) {
        //cluster.workers[id].on('message', (worker, message, handle) => {
        //    urls.concat(message)
        //    console.log(message.cmd)
        //});
        cluster.workers[id].on('listening', () => {
            console.log('echooooo');
            cluster.workers[id].send({ cmd: "echocho" })
        });
    };
    
    cluster.on('message', (worker, message, handle) => {
        console.log(message.cmd);
    });

    //worker.on('listening', () => {
    //    console.log('Worker are listening')
    //});

    //worker.send({cmd: urls.shift()})
    
    //for (const id in cluster.workers) {
    //    cluster.workers[id].on('message', worker.crawl())
    //}
    //worker.on('message', function(message) {
    //    console.log(`Worker ${worker.process.pid} is idle. Get back to work`)
    //});

} else {
    http.createServer((req, res) => {
        res.writeHead(200);
        res.end('hello_world\n');
        let next_items = 'hello'

    }).listen(8000);
        
    console.log(`Worker ${process.pid} started`)
    process.on('message', function(msg) {
        console.log(msg.cmd);
        //next_items = crawler.crawl('hello');
    });

    process.send({ cmd: 'hello' });


}

